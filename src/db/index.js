import Realm from 'realm';
import {
    WALLETS_SCHEMA
} from './Schema';

const databaseOptions = {
    path: 'CHIA.realm',
    schema: [ 
        WALLETS_SCHEMA
    ],
    schemaVersion: 0
}

export default new Realm(databaseOptions);