import React, {useEffect, useContext, useState, useRef} from 'react';
import  {
    withStyles,
    Layout,
    Icon,
    Input,
    TopNavigation,
    Divider, Button, Text
} from '@ui-kitten/components';
import  { View, TouchableOpacity} from 'react-native';
import Clipboard from '@react-native-community/clipboard';
import Snackbar from 'react-native-snackbar';
import { strings } from '../utils/i18n';
import { WalletContext } from '../hooks/useWallet';

const CopySeedPhraseScreenComponent = ({
    eva,
    navigation,
}) => {
    
    const pulseIconRef = React.useRef();
    const [seedPhraseCopied, setSeedPhraseCopied] = useState(false);
    const {wallet, walletDetails, setWallet, setWalletDetails, clearWallet} = useContext(WalletContext)
    React.useEffect(() => {
        if(pulseIconRef && pulseIconRef.current){
            pulseIconRef.current.startAnimation();
        }
        
    }, []);

    const copyToClipBoard = async(data) => {
        await Clipboard.setString(data);
        setSeedPhraseCopied(true);
        if(pulseIconRef && pulseIconRef.current){
            pulseIconRef.current.startAnimation();
        }
        Snackbar.show({
            text: 'Copied to clipboard',
            duration: Snackbar.LENGTH_SHORT,
        });
    }

    return (
        <>
            <TopNavigation 
                style={eva.style.topBar} 
                title={strings('CopySeedPhrase.title')}
                alignment='center'
            />
            <Divider />
            <Layout style={eva.style.container}>
                <View style={eva.style.form}>
                    <Input 
                        style={eva.style.input}
                        value={walletDetails.seed_phrase}
                        disabled
                        multiline
                        size={'large'}
                        textStyle={eva.style.input}
                        caption={strings('CopySeedPhrase.input.seed_phrase.caption')}
                    />
                    <TouchableOpacity 
                        style={eva.style.Clipboard}
                        onPress={() => copyToClipBoard(walletDetails.seed_phrase)}
                    >
                        <Icon 
                            fill={eva.theme['color-primary-600']} 
                            style={{ height: 20, width: 20, marginRight: 10}} 
                            name='clipboard' 
                            ref={pulseIconRef}
                            animation='pulse'
                        />
                        <Text category='s2'>{strings('CopySeedPhrase.input.seed_phrase.copy')}</Text>
                    </TouchableOpacity>
                    <Input 
                        style={eva.style.input}
                        value={walletDetails.XCH_address}
                        disabled
                        multiline
                        size={'large'}
                        textStyle={eva.style.input}
                        caption={strings('CopySeedPhrase.input.wallet.caption')}
                    />
                    <TouchableOpacity 
                        style={eva.style.Clipboard}
                        onPress={() => copyToClipBoard(walletDetails.XCH_address)}
                    >
                        <Icon 
                            
                            fill={eva.theme['color-primary-600']} 
                            style={{ height: 20, width: 20, marginRight: 10}} 
                            name='clipboard' 
                            ref={pulseIconRef}
                            animation='pulse'
                        />
                        <Text category='s2'>{strings('CopySeedPhrase.input.wallet.copy')}</Text>
                    </TouchableOpacity>
                </View>
                <View style={{paddingHorizontal: 30, paddingBottom: 10}}>
                    <View style={[ eva.style.commonContainer, eva.style.B15]}>
                        <Text status='warning' category='label'>{strings('CopySeedPhrase.note')}</Text>
                    </View>
                    <Button 
                        style={eva.style.button}
                        disabled={!seedPhraseCopied}
                        onPress={() => navigation.navigate('MainAppScreen')}
                    >
                        {strings('CopySeedPhrase.goto_button')}
                    </Button>
                </View>
            </Layout>
        </>
    )
}

const CopySeedPhraseScreen = withStyles(CopySeedPhraseScreenComponent, theme => ({
      container: {
        flex: 1,
        overflow: 'scroll'
      },
      topBar: {
          height: 50,
          display: 'flex', justifyContent: 'center'
      },
      form: {
          flex:1,
          top: 20,
          paddingHorizontal: 30
      },
      icon: {
          width: 32,
          height: 32,
          marginRight: 10
      },
      input: {
          fontSize: 20,
          marginBottom: 5
      },
      Clipboard: {
        display: 'flex', 
        flexDirection: 'row', 
        justifyContent: 'flex-end', 
        alignItems: 'center',
        marginBottom: 20
      },
      button:{
        backgroundColor: '#1F51FF',
        width: '100%',
        marginBottom: 20,
        borderColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        elevation: 3,
    },
      commonContainer: {
        display: 'flex', 
        justifyContent: 'center', 
        alignItems: 'center',
      },
      B15: {
          marginBottom: 15
      }
}));

export default CopySeedPhraseScreen;

