import React, {useContext, useEffect} from 'react';
import  {
    withStyles,
    Layout,
    Text,
    TopNavigation,
    Card,
    Divider,
    Toggle,
    Button,
    Modal
} from '@ui-kitten/components';
import { View } from 'react-native';
import {ThemeContext} from '../hooks/useTheme';
import Icon from 'react-native-vector-icons/Ionicons';
import {WalletContext} from '../hooks/useWallet';
import {strings} from '../utils/i18n';
import Snackbar from 'react-native-snackbar';
import Clipboard from '@react-native-community/clipboard'

const SettingScreenScreenComponent = ({
    eva,
    navigation
}) => {
    const [theme, toggleTheme] = useContext(ThemeContext);
    const {wallet, walletDetails, setWallet, setWalletDetails, clearWallet} = useContext(WalletContext)
    const [visible, setVisible] = React.useState(false);

    const remove = async() => {
        await setVisible(false)
        await clearWallet()
    }

    const copyToClipBoard = async(data) => {
        await Clipboard.setString(walletDetails.seed_phrase)
        Snackbar.show({
            text: 'Copied to clipboard',
            duration: Snackbar.LENGTH_SHORT,
        });
    }

    return (
        <>
        <TopNavigation title={strings('SettingsScreen.title')} style={eva.style.topBar} alignment='center' />
        <Divider />
        <Layout style={eva.style.container}>
            <Card style={eva.style.card} appearance='outline'>
                <View style={eva.style.cardItems}>
                    <Icon name={ theme === 'dark' ? 'moon': 'moon-outline'} style={eva.style.icons} />
                    <Text>{strings('SettingsScreen.menus.dark_theme')}</Text>
                    <Toggle checked={theme === 'dark' ? true: false} onChange={() => toggleTheme()} />
                </View>
            </Card>
            {walletDetails && walletDetails.seed_phrase &&
            <Button onPress={() => copyToClipBoard(wallet.seed_phrase)} style={{ marginBottom: 10 }}>
                Copy Seed Phrase
            </Button>}
            {wallet && <Button style={[eva.style.button,{backgroundColor: 'red'}]} onPress={() => setVisible(true)}>
                <Text>{strings('SettingsScreen.menus.remove_wallet')}</Text>
            </Button>}
            <Button style={eva.style.button} onPress={() => navigation.navigate("FromStartScreen")}>
                {strings('SettingsScreen.menus.add_wallet')}
            </Button>
        </Layout>
            <Modal
                visible={visible}
                backdropStyle={eva.style.backdrop}
                onBackdropPress={() => setVisible(false)}
            >
                <Card disabled={true}>
                <Text
                    style={{ textAlign: 'center', marginBottom: 10 }}
                >
                    {strings('SettingsScreen.remove_prompt.title')}
                </Text>
                <Text 
                    status='warning'
                    style={{ textAlign: 'center', marginBottom: 10 }}
                >
                    {strings('SettingsScreen.remove_prompt.note')}
                </Text>
                
                <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around' }}>
                    <Button style={[eva.style.button,{backgroundColor: 'red'}]} onPress={() => remove()}>
                    {strings('SettingsScreen.remove_prompt.buttons.remove')}
                    </Button>
                    <Button style={[eva.style.button]} onPress={() => setVisible(false)}>
                    {strings('SettingsScreen.remove_prompt.buttons.dismiss')}
                    </Button>
                </View>
                </Card>
            </Modal>
        </>
    )
}

const SettingScreen = withStyles(SettingScreenScreenComponent, theme => ({
    container: {
      flex: 1,
      paddingHorizontal: 15,
    },
    topBar: {
        elevation: 2
    },
    card: {
        display: 'flex',
        flexDirection: 'row',
        marginVertical: 15,
        width: "100%"
    },
    cardItems: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: "100%",
        alignItems: 'center'
    },
    icons: {
        fontSize: 20
    },
    backdrop: {
        backgroundColor: 'white',
    },
    button:{
        backgroundColor: '#1F51FF',
        marginVertical: 20,
        borderColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        elevation: 3,
    }
}));

export default SettingScreen;

