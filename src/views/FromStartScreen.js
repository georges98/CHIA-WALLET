import React, { useContext, useEffect } from 'react';
import  {
    withStyles,
    Layout,
    Text,
    Button,
    Icon, 
    TopNavigationAction,
} from '@ui-kitten/components';
import { SvgCssUri } from 'react-native-svg';
import { WalletContext } from '../hooks/useWallet';
import {strings} from '../utils/i18n'
const FromStartComponent = ({
    eva,
    navigation
}) => {
    const {wallet} = useContext(WalletContext)
   
    return (
        <Layout style={eva.style.container}>
            <SvgCssUri 
                height={200}
                width={200}
                uri= 'https://www.chia.net/img/chia_logo.svg'
            />
            <Button 
                onPress={() => navigation.navigate('NewWalletScreen')} 
                size='large' 
                style={eva.style.createNewWallet }
            >
                    <Text style={{ color: "white" }} category='label'>{strings('FromStartScreen.create_button')}</Text>
            </Button>
            <Button 
                onPress={() => navigation.navigate('RestoreWalletScreen')} 
                size='large' 
                style={eva.style.restoreWallet}
            >
                    <Text  style={{ color: "white" }} category='label'>{strings('FromStartScreen.restore_button')}</Text>
            </Button>
            <Button 
                onPress={() => navigation.navigate('MainAppScreen')} 
                size='large' 
                style={eva.style.restoreWallet}
            >
                    <Text style={{color: 'white'}} category='label'>{strings('FromStartScreen.enter_button')}</Text>
            </Button>
        </Layout>
    )
}

const FromStartScreen = withStyles(FromStartComponent, theme => ({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      width:'100%',
      paddingLeft: 20,
      paddingRight: 20
    },
    createNewWallet: {
        backgroundColor: '#1F51FF',
        width: '100%',
        marginBottom: 20,
        borderColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        elevation: 3,
    },
    restoreWallet: {
        backgroundColor:theme['color-primary-100'],
        width: '100%',
        marginBottom: 10,
        borderColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        elevation: 3,
    }
}));

export default FromStartScreen;

