import React, {useContext, useEffect, useState} from 'react';
import  {
    withStyles,
    Layout,
    BottomNavigation, BottomNavigationTab, Icon,
     Text,
     Spinner,
     Button
} from '@ui-kitten/components';
import {
    ScrollView,
    RefreshControl
} from 'react-native';
import {WalletContext} from '../hooks/useWallet'
import {strings} from '../utils/i18n';
import WalletCard from './components/SingleWalletCard';
import { SvgCssUri } from 'react-native-svg';

const MainAppScreenComponent = ({
    eva,
    navigation
}) => {
    const pulseIconRef = React.useRef();
    React.useEffect(() => {
        if(pulseIconRef.current){
            pulseIconRef.current.startAnimation();
        }
    }, []);

    const {wallet, walletDetails} = useContext(WalletContext);
    const [refresh, setRefresh] = useState(false)

    const WalletsRenderer = () => walletDetails.map((item, idx) => (
        <WalletCard key={`wallet-${idx}`} eva={eva} data={item} />
    ))

    return (
        <>
            {wallet && 
                <Layout style={eva.style.container}>
                    <ScrollView 
                        refreshControl={
                            <RefreshControl
                                refreshing={refresh}
                                onRefresh={() => {
                                    setRefresh(true)
                                    setTimeout(() => {
                                        setRefresh(false)
                                    }, 3000)
                                }}
                                title="Loading..."
                            />
                        }
                        style={eva.style.viewContainer}>
                        {walletDetails.length && 
                            <WalletsRenderer />
                        }
                    </ScrollView>
            </Layout>}
            {!wallet && 
                <Layout style={eva.style.noWalletContainer}>
                    <Text category='h3'>{strings("MainAppScreen.no_wallet")}</Text>
                    <Button style={eva.style.button} onPress={() => navigation.navigate("FromStartScreen")}>
                        {strings('SettingsScreen.menus.add_wallet')}
                    </Button>
                </Layout>
            }
        </>
    )
}

const MainAppScreen = withStyles(MainAppScreenComponent, theme => ({
    container: {
      flex: 1,
      
    },
    noWalletContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    topBar: {
        backgroundColor: theme['color-primary-100']
    },
    bottomNavigation: {
        paddingVertical: 8,
        height: 20
    },
    icon: {
        width: 10,
        height: 10,
        color: theme['color-primary-100'],
    },
    viewContainer: {
        flex: 1,
        top: 20,
        paddingHorizontal: 20
    },
    walletContainer: {
        marginTop: 40,
        display: 'flex',
        padding: 10,
        elevation: 2,
    },
    button:{
        backgroundColor: '#1F51FF',
        marginVertical: 20,
        borderColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        elevation: 3,
    },
    card: {
        display: 'flex',
        flexDirection :'row',
        // margin: 2,
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottom: 2,
        borderColor: theme['color-primary-100']
    }
}));

export default MainAppScreen;

