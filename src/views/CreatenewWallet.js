import React, { useContext, useState } from 'react';
import  {
    withStyles,
    Layout,
    Text,
    Icon, 
    TopNavigation,
    Input, Button, Divider, Spinner
} from '@ui-kitten/components';
import {View} from 'react-native';
import {
    Formik,
} from 'formik';
import {CreateNewWallet} from '../connections/BackendApi'
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { WalletContext } from '../hooks/useWallet';
import {strings} from '../utils/i18n'

const NewWalletScreenComponent = ({
    eva,
    navigation
}) => {
    const {walletSetter} = useContext(WalletContext)
    const Action = async(values, actions) => {
        const resp = await CreateNewWallet();
        return resp
    }
    
    return (
        <>
            <TopNavigation
                accessoryLeft={() =>
                    navigation.canGoBack() ?
                    <Icon
                        style={eva.style.icon}
                        fill={eva.theme['color-primary-300']}
                        name='arrow-back'
                        onPress={() => navigation.goBack()}
                    />: null
                } 
                style={eva.style.topBar} 
                title={strings('CreateNewWallet.title')}
            />
            <Divider />
            <Layout style={eva.style.container}>
                <View style={eva.style.form}>
                    <View style={eva.style.header}>
                        <Icon
                            style={eva.style.icon}
                            fill={eva.theme['color-primary-300']}
                            name='shield'
                        />
                        <Text style={{fontSize: 20}} category='h6'>
                            New wallet
                        </Text>
                    </View>
                    <View>
                        <Formik
                            initialValues={{ password: '', passwordConfirmation: '' }}
                            onSubmit={async(values, actions) =>  {
                                const walletDetails = await Action(values, actions);
                                await actions.setSubmitting(false);
                                if(walletDetails){
                                    await walletSetter(walletDetails);
                                    await navigation.push('CopySeedPhraseScreen')
                                }
                                else{
                                    Snackbar.show({
                                        text: strings('CreateNewWallet.Snackbar.failed'),
                                        duration: Snackbar.LENGTH_LONG,
                                        textColor: 'red',
                                        action: {
                                            text: strings('CreateNewWallet.Snackbar.action_button'),
                                            textColor: 'yellow',
                                            onPress: () => {},
                                        },
                                    });
                                }
                            }}
                        >
                            {({
                                handleSubmit,
                                isSubmitting
                            }) => (
                                <>  
                                <Button style={eva.style.button} disabled={isSubmitting} onPress={handleSubmit} accessoryLeft={() => isSubmitting ? <Spinner size='small' style={{color: 'white'}}/>: null}>
                                    {strings('CreateNewWallet.create')}
                                </Button>
                                 {isSubmitting && <Text status='warning' category='c1' style={{ textAlign: 'center' }}>
                                    {strings('CreateNewWallet.load_message')}
                                </Text>}
                                </>
                            )}
                         </Formik>
                    </View>
                </View>
                
            </Layout>
        </>
    )
}

const NewWalletScreen = withStyles(NewWalletScreenComponent, theme => ({
    container: {
      flex: 1,
    },
    topBar: {
        backgroundColor: theme['color-primary-100'],
        height: 50,
        display: 'flex', justifyContent: 'center'
    },
    form: {
        flex:1,
        top: 80,
        paddingHorizontal: 30
    },
    header: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 20
    },
    icon: {
        width: 32,
        height: 32,
        marginRight: 10
    },
    input: {
        marginBottom: 15
    },
    button:{
        backgroundColor: '#1F51FF',
        width: '100%',
        marginBottom: 20,
        borderColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        elevation: 3,
    },
    inputError: {
        color: theme['color-danger-900']
    }
}));

export default NewWalletScreen;

