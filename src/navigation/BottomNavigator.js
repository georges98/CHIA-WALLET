import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import {Text} from '@ui-kitten/components'
import  IonIcon from 'react-native-vector-icons/Ionicons';

// Screens
import MainAppScreen from '../views/MainAppScreen';
import SettingsScreen from '../views/SettingsScreen';

import {strings} from '../utils/i18n'

const DashboardBottomTab = createMaterialTopTabNavigator();

function DashboardTab() {
  return (
    <DashboardBottomTab.Navigator
        lazy={true}
        keyboardDismissMode={'on-drag'}
        tabBarPosition='bottom'
        tabBarOptions= {{ 
            showIcon: true,
            activeTintColor: '#1F51FF'
        }}
    >
        <DashboardBottomTab.Screen  
            name="Wallets" 
            component={MainAppScreen} 
            options={{
                tabBarIcon: ({ focused, color }) => <IonIcon name='wallet' style={{ fontSize: 20, color: color}} />,
                tabBarLabel: ({ color}) => <Text category='s2' style={{ color: color}}>{strings('BottomTabs.wallet')}</Text>
            }}
        />
        <DashboardBottomTab.Screen  
            name="Settings" 
            component={SettingsScreen} 
            options={{
                tabBarIcon: ({ color }) => <IonIcon name='settings-outline' style={{ fontSize: 20, color: color}} />,
                tabBarLabel: ({color}) => <Text category='s2' style={{ color: color}}>{strings('BottomTabs.settings')}</Text>
            }}
        />
    </DashboardBottomTab.Navigator>
  );
}

export default DashboardTab;