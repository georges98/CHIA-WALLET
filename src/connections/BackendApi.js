import Axios from 'axios';
import Snackbar from 'react-native-snackbar'

const BackendInstance = Axios.create({
    baseURL: 'https://185.69.167.141:32253'
    
})

export const CreateNewWallet = async() => {
   
    try{
        const {data} = await BackendInstance.get('/keyGen')
        
        return data
    }
    catch(err){
        console.log(err);
        return null
    }
}

export const GetBalance = async(seedPhrase) => {
    try{
        const {data} = await BackendInstance.get(`/balance?seed_phrase=${seedPhrase}`)
        return data
    }
    catch(err){
        return null
    }
    
}

export const RestoreWallet = async(seedPhrase) => {
    try{
        const {data} = await BackendInstance.get(`/restore?seed_phrase=${seedPhrase}`)
        return data
    }
    catch(err){
        return null
    }
}

export const GetBalanceByFingerPrint = async(fingerPrintNo) => {

           
    try{
        const {data} = await BackendInstance.get(`/balanceN?fingerprintNo=${fingerPrintNo}`)
        console.log(data)
        return data
    }
    catch(err){
        console.log(err)
        return null
    }
}


export const GetBalanceByAddress = async(address) => {
        
    const apiO = await fetch('https://api.xchscan.com/v1/graphql', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        query,
        variables: { address: address },
      })
    })
    var apiJ = await apiO.json()
    balance = Number(apiJ.data.balance.aggregate.sum.amount) / 1000000000000;
    console.log(balance);
    return balance
  }